module heichling

go 1.22

require (
	github.com/GoogleChrome/workbox v7.1.0+incompatible // indirect
	github.com/hugomods/icons v0.6.3 // indirect
	github.com/hugomods/idb-js v0.1.0 // indirect
	github.com/hugomods/pwa v0.9.0 // indirect
	github.com/twbs/icons v1.11.3 // indirect
)
